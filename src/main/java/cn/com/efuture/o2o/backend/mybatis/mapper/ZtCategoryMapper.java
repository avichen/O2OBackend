package cn.com.efuture.o2o.backend.mybatis.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import cn.com.efuture.o2o.backend.mybatis.entity.Dept;
import cn.com.efuture.o2o.backend.mybatis.entity.ZtCategory;
import cn.com.efuture.o2o.backend.mybatis.entity.ZtCategoryLog;

@Repository
@Mapper
public interface ZtCategoryMapper {

    List<ZtCategory> getZtCategoryList(Map<String, Object> map);
	
	ZtCategory getZtCategory(Map<String, Object> map);
	
	void updateZtCategory(ZtCategory ztCategory);
	
	void insertZtCategory(ZtCategory ztCategory);
	
	void insertZtCategoryLog(ZtCategoryLog ztCategoryLog);
	
	int getZtCategoryIdCount(ZtCategory ztCategory);
	
	int getSeqNoMax(ZtCategory ztCategory);
	
	//获取业务系统类别
    Dept getDept(Map<String, Object> map);
}
