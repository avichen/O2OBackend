package cn.com.efuture.o2o.backend.mybatis.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import cn.com.efuture.o2o.backend.mybatis.entity.ZtLabel;
import cn.com.efuture.o2o.backend.mybatis.entity.ZtLabelType;

@Repository
@Mapper
public interface ZtLabelMapper {

	List<ZtLabel> getZtLabelList(Map<String, Object> map);
	
	List<ZtLabelType> getZtLabelTypeList(Map<String, Object> map);
	
	ZtLabel getZtLabel(ZtLabel ztLabel);
	
	void insertZtLabel(ZtLabel ztLabel);
	
	void updateZtLabel(ZtLabel ztLabel);
	
	void deleteZtLabel(ZtLabel ztLabel);
}
