package cn.com.efuture.o2o.backend.mybatis.mapper;

import cn.com.efuture.o2o.backend.mybatis.entity.ZtProduct;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ZtProductMapper extends Serializable {

    List<ZtProduct> getZtProductList(Map<String, Object> map);


    long getZtProductListCount(Map<String, Object> map);

    ZtProduct getZtProductByItemId(Map<String, Object> map);

    List<Map> getZtProductImagesList(Map<String, Object> map);

    List<Map> getZtProductLabelList(Map<String, Object> map);

    int updateZtProduct(ZtProduct ztProduct);

    void deleteZtProductLabelList(int itemId);

    void insertZtProductLabelList(Map<String,Object> map);

    List<Map<String,Object>> getZtSkuList(Map<String,Object> map);

    long getZtSkuListCount(Map<String, Object> map);
}
