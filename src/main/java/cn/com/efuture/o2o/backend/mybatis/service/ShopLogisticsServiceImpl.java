package cn.com.efuture.o2o.backend.mybatis.service;

import cn.com.efuture.o2o.backend.mybatis.entity.ShopLogisticsTime;
import cn.com.efuture.o2o.backend.mybatis.entity.ShopLogistics;
import cn.com.efuture.o2o.backend.mybatis.mapper.ShopLogisticsMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ShopLogisticsServiceImpl {

    private final ShopLogisticsMapper shopLogisticsMapper;

    public ShopLogisticsServiceImpl(ShopLogisticsMapper shopLogisticsMapper) {
        this.shopLogisticsMapper = shopLogisticsMapper;
    }


    public List<ShopLogistics> getShopLogisticsList(Map<String, Object> map){
        return  shopLogisticsMapper.getShopLogisticsList(map);
    }

    public List<ShopLogisticsTime> getShopLogisticsTimeList(Map<String,Object> map) {
       return shopLogisticsMapper.getShopLogisticsTimeList(map);
    }

    public void insertShopLogistics(Map<String, Object> map) {
    	ShopLogistics sl = shopLogisticsMapper.getShopLogistics(map);
    	if(sl!=null) {throw new RuntimeException("该门店物流已存在！");}
        shopLogisticsMapper.insertShopLogistics(map);
    }
    
    public void insertShopLogisticsTime(Map<String, Object> map) {
        shopLogisticsMapper.insertShopLogisticsTime(map);
    }
    
    public void updateShopLogistics(Map<String, Object> map) {
        shopLogisticsMapper.updateShopLogistics(map);
    }
    
    public void updateShopLogisticsTime(Map<String, Object> map) {
        shopLogisticsMapper.updateShopLogisticsTime(map);
    }
    
    public void deleteShopLogisticsTime(Map<String, Object> map) {
        shopLogisticsMapper.deleteShopLogisticsTime(map);
    }
}
