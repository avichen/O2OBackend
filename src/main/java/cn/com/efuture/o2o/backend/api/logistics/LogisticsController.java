package cn.com.efuture.o2o.backend.api.logistics;

import cn.com.efuture.o2o.backend.mybatis.entity.LogisticsFreeShipping;
import cn.com.efuture.o2o.backend.mybatis.entity.LogisticsType;
import cn.com.efuture.o2o.backend.mybatis.service.LogisticsServiceImpl;
import cn.com.efuture.o2o.backend.system.JsonResponse;
import cn.com.efuture.o2o.backend.util.ParameterHelper;

import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 *  物流相关api
 */
@RestController
@RequestMapping("/api/logistics")
public class LogisticsController {
    protected org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    private final LogisticsServiceImpl logisticsService;

    public LogisticsController(LogisticsServiceImpl logisticsService) {
        this.logisticsService = logisticsService;
    }

    // 获取所有物流类型
    @RequestMapping(value = "/getLogisticsTypeList", method = RequestMethod.GET)
    public JsonResponse getLogisticsTypeList(@RequestParam(value = "data")String data) {
    	Map<String, Object> map = JSONObject.parseObject(data);
        try {
            logger.info("------------getLogisticsTypeList-----------");
            ParameterHelper.cookPageInfo(map);
            //设置分页信息
            PageHelper.startPage((int) map.get("page"), (int) map.get("pageSize"));
            //执行查询
            List<LogisticsType> list = logisticsService.getLogisticsTypeList(map);
            //获取查询结果
            PageInfo<LogisticsType> pageInfo = new PageInfo<>(list);
            return JsonResponse.ok(pageInfo.getTotal(),list);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }

    // 新增物流类型
    @RequestMapping(value = "/insertLogisticsType", method = RequestMethod.POST)
    public JsonResponse insertLogisticsType(@RequestParam(value = "data")String data) {
    	LogisticsType logisticsType = JSONObject.parseObject(data, LogisticsType.class);
        try {
            logger.info("------------insertLogisticsType-----------");
            logisticsService.insertLogisticsType(logisticsType);
            return JsonResponse.ok("新增成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
    
    // 修改物流类型
    @RequestMapping(value = "/updateLogisticsType", method = RequestMethod.POST)
    public JsonResponse updateLogisticsType(@RequestParam(value = "data")String data) {
    	LogisticsType logisticsType = JSONObject.parseObject(data, LogisticsType.class);
        try {
            logger.info("------------insertLogisticsType-----------");
            logisticsService.updateLogisticsType(logisticsType);
            return JsonResponse.ok("修改成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }

    // 根据物流类型获取免邮规则
    @RequestMapping(value = "/geLogisticsFreeShippingList", method = RequestMethod.GET)
    public JsonResponse geLogisticsFreeShippingList(@RequestParam(value = "data")String data) {
    	Map<String, Object> map = JSONObject.parseObject(data);
        try {
            logger.info("------------geLogisticsFreeShippingList-----------");
           /* ParameterHelper.cookPageInfo(map);
            //设置分页信息
            PageHelper.startPage((int) map.get("page"), (int) map.get("pageSize"));*/
            //执行查询
            List<LogisticsFreeShipping> list = logisticsService.getLogisticsFreeShippingList(map);
            //获取查询结果
            //PageInfo<LogisticsFreeShipping> pageInfo = new PageInfo<>(list);
            return JsonResponse.ok(list);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
    
    // 新增免邮规则
    @RequestMapping(value = "/insertLogisticsFreeShipping", method = RequestMethod.POST)
    public JsonResponse insertLogisticsFreeShipping(@RequestParam(value = "data")String data) {
    	LogisticsFreeShipping logisticsFreeShipping = JSONObject.parseObject(data, LogisticsFreeShipping.class);
        try {
            logger.info("------------insertLogisticsFreeShipping-----------");
            logisticsService.insertLogisticsFreeShipping(logisticsFreeShipping);
            return JsonResponse.ok("新增成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
    
    // 修改免邮规则
    @RequestMapping(value = "/updateLogisticsFreeShipping", method = RequestMethod.POST)
    public JsonResponse updateLogisticsFreeShipping(@RequestParam(value = "data")String data) {
    	LogisticsFreeShipping logisticsFreeShipping = JSONObject.parseObject(data, LogisticsFreeShipping.class);
        try {
            logger.info("------------updateLogisticsFreeShipping-----------");
            logisticsService.updateLogisticsFreeShipping(logisticsFreeShipping);
            return JsonResponse.ok("修改成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
    
    // 删除免邮规则
    @RequestMapping(value = "/deleteLogisticsFreeShipping", method = RequestMethod.POST)
    public JsonResponse deleteLogisticsFreeShipping(@RequestParam(value = "data")String data) {
    	LogisticsFreeShipping logisticsFreeShipping = JSONObject.parseObject(data, LogisticsFreeShipping.class);
        try {
            logger.info("------------deleteLogisticsFreeShipping-----------");
            logisticsService.deleteLogisticsFreeShipping(logisticsFreeShipping);
            return JsonResponse.ok("删除成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }

}
