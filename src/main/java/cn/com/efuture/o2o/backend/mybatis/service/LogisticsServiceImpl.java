package cn.com.efuture.o2o.backend.mybatis.service;

import cn.com.efuture.o2o.backend.mybatis.entity.LogisticsFreeShipping;
import cn.com.efuture.o2o.backend.mybatis.entity.LogisticsType;
import cn.com.efuture.o2o.backend.mybatis.mapper.LogisticsMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LogisticsServiceImpl {

    private final LogisticsMapper logisticsMapper;

    public LogisticsServiceImpl(LogisticsMapper logisticsMapper) {
        this.logisticsMapper = logisticsMapper;
    }


    public List<LogisticsType> getLogisticsTypeList(Map<String, Object> map){
        return  logisticsMapper.getLogisticsTypeList(map);
    }
    
    public void insertLogisticsType(LogisticsType logisticsType){
    	LogisticsType lt = logisticsMapper.getLogisticsType(logisticsType);
    	if(lt!=null) {throw new RuntimeException("该物流类型已存在！");}
    	logisticsMapper.insertLogisticsType(logisticsType);
    }
    
    public void updateLogisticsType(LogisticsType logisticsType){
    	logisticsMapper.updateLogisticsType(logisticsType);
    }

    public List<LogisticsFreeShipping> getLogisticsFreeShippingList(Map<String, Object> map) {
        return logisticsMapper.getLogisticsFreeShippingList(map);
    }
    
    public void insertLogisticsFreeShipping(LogisticsFreeShipping logisticsFreeShipping){
    	LogisticsFreeShipping ls = logisticsMapper.getLogisticsFreeShipping(logisticsFreeShipping);
    	if(ls!=null) {
    		throw new RuntimeException("该免邮规则已存在！");
    	}
    	logisticsMapper.insertLogisticsFreeShipping(logisticsFreeShipping);
    }
    
    public void updateLogisticsFreeShipping(LogisticsFreeShipping logisticsFreeShipping){
    	logisticsMapper.updateLogisticsFreeShipping(logisticsFreeShipping);
    }
    
    public void deleteLogisticsFreeShipping(LogisticsFreeShipping logisticsFreeShipping){
    	logisticsMapper.deleteLogisticsFreeShipping(logisticsFreeShipping);
    }
}
