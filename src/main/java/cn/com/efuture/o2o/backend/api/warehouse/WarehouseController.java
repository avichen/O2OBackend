package cn.com.efuture.o2o.backend.api.warehouse;

import cn.com.efuture.o2o.backend.mybatis.entity.Warehouse;
import cn.com.efuture.o2o.backend.mybatis.service.WarehouseServiceImpl;
import cn.com.efuture.o2o.backend.system.JsonResponse;
import cn.com.efuture.o2o.backend.util.ParameterHelper;
import cn.com.efuture.o2o.backend.util.SessionHelper;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 前置仓库
 */
@RestController
@RequestMapping("/api/warehouse")
public class WarehouseController {
    protected org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    private final WarehouseServiceImpl warehouseService;

    public WarehouseController(WarehouseServiceImpl warehouseService) {
        this.warehouseService = warehouseService;
    }

    /**
     * 获取仓库商品
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/getWarehouseGoodsList", method = RequestMethod.GET)
    public JsonResponse getWarehouseGoodsList(@RequestParam(value = "data", required = false) String data) {
        try {
            JSONObject map = JSONObject.parseObject(data);
            logger.info("------------getWarehouseGoodsList-----------");
            ParameterHelper.cookPageInfo(map);
            ParameterHelper.cookCityInfo(map);
            map.put("userName", SessionHelper.getUserName());
            long count = ParameterHelper.getCount(map, warehouseService,null);
            // 设置分页信息
            int page = map.getIntValue("page");
            int pageSize = map.getIntValue("pageSize");
            map.put("page", (page - 1) * pageSize);
            map.put("pageSize", pageSize);

            List<Warehouse> list = warehouseService.getWarehouseGoodsList(map);
            //前端虚拟 dom 树需要 key
            int key = 0;
            for (Warehouse warehouse : list) {
                warehouse.setKey(key++);
            }
            return JsonResponse.ok(count, list);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
}
