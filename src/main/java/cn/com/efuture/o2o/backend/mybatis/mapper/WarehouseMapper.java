package cn.com.efuture.o2o.backend.mybatis.mapper;

import cn.com.efuture.o2o.backend.mybatis.entity.Warehouse;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface WarehouseMapper {

    List<Warehouse> getWarehouseGoodsList(Map<String, Object> map);

    long getWarehouseGoodsListCount(Map<String, Object> map);
}
