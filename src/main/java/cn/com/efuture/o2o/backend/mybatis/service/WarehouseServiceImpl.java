package cn.com.efuture.o2o.backend.mybatis.service;

import cn.com.efuture.o2o.backend.mybatis.entity.Warehouse;
import cn.com.efuture.o2o.backend.mybatis.mapper.WarehouseMapper;
import cn.com.efuture.o2o.backend.util.PageCount;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WarehouseServiceImpl implements PageCount {

    private final WarehouseMapper warehouseMapper;

    public WarehouseServiceImpl(WarehouseMapper warehouseMapper) {
        this.warehouseMapper = warehouseMapper;
    }

    public List<Warehouse> getWarehouseGoodsList(Map<String, Object> map) {
        return warehouseMapper.getWarehouseGoodsList(map);
    }

    @Override
    public long getCount(Map<String, Object> map, String type) {
        return warehouseMapper.getWarehouseGoodsListCount(map);
    }
}
