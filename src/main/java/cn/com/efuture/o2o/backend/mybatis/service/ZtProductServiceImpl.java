package cn.com.efuture.o2o.backend.mybatis.service;

import cn.com.efuture.o2o.backend.mybatis.entity.ZtProduct;
import cn.com.efuture.o2o.backend.mybatis.mapper.ZtProductMapper;
import cn.com.efuture.o2o.backend.util.PageCount;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ZtProductServiceImpl implements PageCount {

    private final ZtProductMapper ztProductMapper;


    public ZtProductServiceImpl(ZtProductMapper ztProductMapper) {
        this.ztProductMapper = ztProductMapper;
    }


    public List<ZtProduct> getZtProductList(Map<String, Object> map) {
        return ztProductMapper.getZtProductList(map);
    }

    @Override
    public long getCount(Map<String, Object> map, String type) {
        if ("getZtProductList".equals(type))
            return ztProductMapper.getZtProductListCount(map);
        else if("getZtSkuList".equals(type)){
            return ztProductMapper.getZtSkuListCount(map);
        }else {
            return 0;
        }

    }

    public ZtProduct getZtProductByItemId(Map<String, Object> map) {
        return ztProductMapper.getZtProductByItemId(map);
    }

    public List<Map> getZtProductImagesList(Map<String, Object> map) {
        return ztProductMapper.getZtProductImagesList(map);
    }

    public List<Map> getZtProductLabelList(Map<String, Object> map) {
        return ztProductMapper.getZtProductLabelList(map);
    }

    public int updateZtProduct(ZtProduct ztProduct) {
        return ztProductMapper.updateZtProduct(ztProduct);
    }

    public void saveZtProductLabelList(JSONObject map) {
        int itemId = map.getIntValue("itemId");
        ztProductMapper.deleteZtProductLabelList(itemId);
        JSONArray labelList = map.getJSONArray("labelList");
        if (labelList.size() > 0) {
            ztProductMapper.insertZtProductLabelList(map);
        }
    }

    public List<Map<String, Object>> getZtSkuList(JSONObject map) {
        return ztProductMapper.getZtSkuList(map);
    }
}

