package cn.com.efuture.o2o.backend.api.shopLogistics;

import cn.com.efuture.o2o.backend.mybatis.entity.ShopLogisticsTime;
import cn.com.efuture.o2o.backend.mybatis.entity.ShopLogistics;
import cn.com.efuture.o2o.backend.mybatis.service.ShopLogisticsServiceImpl;
import cn.com.efuture.o2o.backend.system.JsonResponse;
import cn.com.efuture.o2o.backend.util.ParameterHelper;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 门店物流
 */
@RestController
@RequestMapping("/api/shopLogistics")
public class ShopLogisticsController {

    protected org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ShopLogisticsServiceImpl shopLogisticsService;

    public ShopLogisticsController(ShopLogisticsServiceImpl shopLogisticsService) {
        this.shopLogisticsService = shopLogisticsService;
    }

    /**
     *  获取门店物流列表
     *
     * @return shopLogisticsList
     */
    @RequestMapping(value = "/getShopLogisticsList", method = RequestMethod.GET)
    public JsonResponse getShopLogisticsList(@RequestParam(value = "data") String data) {
    	Map<String, Object> map = JSONObject.parseObject(data);
        try {
            logger.info("------------getShopLogisticsList-----------");
            ParameterHelper.cookPageInfo(map);
            ParameterHelper.cookCityInfo(map);
            //设置分页信息
            PageHelper.startPage((int) map.get("page"), (int) map.get("pageSize"));
            //执行查询
            List<ShopLogistics> list = shopLogisticsService.getShopLogisticsList(map);
            //获取查询结果
            PageInfo<ShopLogistics> pageInfo = new PageInfo<>(list);
            return JsonResponse.ok(pageInfo.getTotal(),list);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }

    /**
     *  新增门店物流
     * @param data shopLogistics
     * @return JsonResponse
     */
    @RequestMapping(value = "/insertShopLogistics", method = RequestMethod.POST)
    public JsonResponse insertShopLogistics(@RequestParam(value = "data") String data) {
        try {
            logger.info("------------insertShopLogistics-----------");
            Map<String, Object> map = JSONObject.parseObject(data);
            shopLogisticsService.insertShopLogistics(map);
            return JsonResponse.ok("新增成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
    
    
    /**
     *  修改门店物流
     * @param data shopLogistics
     * @return JsonResponse
     */
    @RequestMapping(value = "/updateShopLogistics", method = RequestMethod.POST)
    public JsonResponse updateShopLogistics(@RequestParam(value = "data") String data) {
        try {
            logger.info("------------updateShopLogistics-----------");
            Map<String, Object> map = JSONObject.parseObject(data);
            shopLogisticsService.updateShopLogistics(map);
            return JsonResponse.ok("修改成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }


    /**
     *  获取门店物时间流列表
     *
     * @return ShopLogisticsTime
     */
    @RequestMapping(value = "/getShopLogisticsTimeList", method = RequestMethod.GET)
    public JsonResponse getShopLogisticsTimeList(@RequestParam(value = "data")String data) {
        try {
            logger.info("------------getLogisticsTimeList-----------");
            Map<String, Object> map = JSONObject.parseObject(data);
            ParameterHelper.cookPageInfo(map);
            //设置分页信息
            PageHelper.startPage((int) map.get("page"), (int) map.get("pageSize"));
            //执行查询
            List<ShopLogisticsTime> list = shopLogisticsService.getShopLogisticsTimeList(map);
            PageInfo<ShopLogisticsTime> pageInfo = new PageInfo<>(list);
            return JsonResponse.ok(pageInfo.getTotal(),list);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }


    /**
     * 新增门店物流时间
     *
     */
    @RequestMapping(value = "/insertShopLogisticsTime", method = RequestMethod.POST)
    public JsonResponse insertShopLogisticsTime(@RequestParam(value = "data")String data) {
        try {
            logger.info("------------insertShopLogisticsTime-----------");
            Map<String, Object> map = JSONObject.parseObject(data);
            shopLogisticsService.insertShopLogisticsTime(map);
            return JsonResponse.ok("新增成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
    
    /**
     *  修改门店物流时间
     * @param data shopLogistics
     * @return JsonResponse
     */
    @RequestMapping(value = "/updateShopLogisticsTime", method = RequestMethod.POST)
    public JsonResponse updateShopLogisticsTime(@RequestParam(value = "data") String data) {
        try {
            logger.info("------------updateShopLogisticsTime-----------");
            Map<String, Object> map = JSONObject.parseObject(data);
            shopLogisticsService.updateShopLogisticsTime(map);
            return JsonResponse.ok("修改成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
    
    // 删除门店物流时间
    @RequestMapping(value = "/deleteShopLogisticsTime", method = RequestMethod.POST)
    public JsonResponse deleteShopLogisticsTime(@RequestParam(value = "data")String data) {
    	Map<String, Object> map = JSONObject.parseObject(data);
        try {
            logger.info("------------deleteShopLogisticsTime-----------");
            shopLogisticsService.deleteShopLogisticsTime(map);
            return JsonResponse.ok("删除成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
    
}
