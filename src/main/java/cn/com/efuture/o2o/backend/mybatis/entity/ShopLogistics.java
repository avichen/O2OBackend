package cn.com.efuture.o2o.backend.mybatis.entity;



public class ShopLogistics {
    private String shopId;
    
    private String shopName;

    private String logisticsType;

    private Integer firstWeight;

    private Double firstFee;

    private Double nextFee;

    private Integer limitWeight;
    
    private Double sFee;

    private Integer isToday;

    private Integer usedTime;

    private Integer choseDays;

    private String gid;
    
    private String gids;
    private String ShopNameAlias;
    private String latitude;
    private String longitude;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getLogisticsType() {
        return logisticsType;
    }

    public void setLogisticsType(String logisticsType) {
        this.logisticsType = logisticsType;
    }

    public Integer getFirstWeight() {
        return firstWeight;
    }

    public void setFirstWeight(Integer firstWeight) {
        this.firstWeight = firstWeight;
    }

    public Double getFirstFee() {
        return firstFee;
    }

    public void setFirstFee(Double firstFee) {
        this.firstFee = firstFee;
    }

    public Double getNextFee() {
        return nextFee;
    }

    public void setNextFee(Double nextFee) {
        this.nextFee = nextFee;
    }

    public Integer getLimitWeight() {
        return limitWeight;
    }

    public void setLimitWeight(Integer limitWeight) {
        this.limitWeight = limitWeight;
    }

    public Integer getIsToday() {
        return isToday;
    }

    public void setIsToday(Integer isToday) {
        this.isToday = isToday;
    }

    public Integer getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(Integer usedTime) {
        this.usedTime = usedTime;
    }

    public Integer getChoseDays() {
        return choseDays;
    }

    public void setChoseDays(Integer choseDays) {
        this.choseDays = choseDays;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

	public String getGids() {
		return gids;
	}

	public void setGids(String gids) {
		this.gids = gids;
	}

	public Double getsFee() {
		return sFee;
	}

	public void setsFee(Double sFee) {
		this.sFee = sFee;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopNameAlias() {
		return ShopNameAlias;
	}

	public void setShopNameAlias(String shopNameAlias) {
		ShopNameAlias = shopNameAlias;
	}
}