package cn.com.efuture.o2o.backend.mybatis.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import cn.com.efuture.o2o.backend.mybatis.entity.ZtLabel;
import cn.com.efuture.o2o.backend.mybatis.entity.ZtLabelType;
import cn.com.efuture.o2o.backend.mybatis.mapper.ZtLabelMapper;

@Service
public class ZtLabelServiceImpl {

	private final ZtLabelMapper ztLabelMapper;
	
	public ZtLabelServiceImpl(ZtLabelMapper ztLabelMapper) {
		super();
		this.ztLabelMapper=ztLabelMapper;
	}
	
	public List<ZtLabel> getZtLabelList(Map<String, Object> map){
		return ztLabelMapper.getZtLabelList(map);
	}
	
	public ZtLabel getZtLabel(ZtLabel ztLabel) {
		return ztLabelMapper.getZtLabel(ztLabel);
	}
	
	public void insertZtLabel(ZtLabel ztLabel) {
		ztLabelMapper.insertZtLabel(ztLabel);
	}
	
	public void updateZtLabel(ZtLabel ztLabel) {
		ztLabelMapper.updateZtLabel(ztLabel);
	}
	
	public void deleteZtLabel(ZtLabel ztLabel) {
		ztLabelMapper.deleteZtLabel(ztLabel);
	}
	
	public List<ZtLabelType> getZtLabelTypeList(Map<String, Object> map){
		 List<ZtLabelType> list = ztLabelMapper.getZtLabelTypeList(map);
		 for (ZtLabelType ztLabelType : list) {
			 map.put("labelTypeId", ztLabelType.getLabelTypeId());
			 List<ZtLabel> listLabel = ztLabelMapper.getZtLabelList(map);
			 ztLabelType.setZtLabelList(listLabel);
		}
		return list;
	}
}
