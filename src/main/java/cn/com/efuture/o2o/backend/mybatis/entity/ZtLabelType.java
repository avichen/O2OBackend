package cn.com.efuture.o2o.backend.mybatis.entity;

import java.util.List;

public class ZtLabelType {

	private  String labelTypeId;
	private  String labelTypeName;
	private List<ZtLabel> ztLabelList;
	
	public String getLabelTypeId() {
		return labelTypeId;
	}
	public void setLabelTypeId(String labelTypeId) {
		this.labelTypeId = labelTypeId;
	}
	public String getLabelTypeName() {
		return labelTypeName;
	}
	public void setLabelTypeName(String labelTypeName) {
		this.labelTypeName = labelTypeName;
	}
	public List<ZtLabel> getZtLabelList() {
		return ztLabelList;
	}
	public void setZtLabelList(List<ZtLabel> ztLabelList) {
		this.ztLabelList = ztLabelList;
	}
	
}
