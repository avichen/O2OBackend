package cn.com.efuture.o2o.backend.mybatis.mapper;

import cn.com.efuture.o2o.backend.mybatis.entity.LogisticsFreeShipping;
import cn.com.efuture.o2o.backend.mybatis.entity.LogisticsType;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface LogisticsMapper {

    List<LogisticsType> getLogisticsTypeList(Map<String, Object> map);
    
    LogisticsType getLogisticsType(LogisticsType logisticsType);
    
    void insertLogisticsType(LogisticsType logisticsType);
    
    void updateLogisticsType(LogisticsType logisticsType);

    List<LogisticsFreeShipping> getLogisticsFreeShippingList(Map<String, Object> map);
    
    LogisticsFreeShipping getLogisticsFreeShipping(LogisticsFreeShipping logisticsFreeShipping); 
    
    void insertLogisticsFreeShipping(LogisticsFreeShipping logisticsFreeShipping);
    
    void updateLogisticsFreeShipping(LogisticsFreeShipping logisticsFreeShipping);
    
    void deleteLogisticsFreeShipping(LogisticsFreeShipping logisticsFreeShipping);
}