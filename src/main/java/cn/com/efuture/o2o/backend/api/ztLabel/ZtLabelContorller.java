package cn.com.efuture.o2o.backend.api.ztLabel;

import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import cn.com.efuture.o2o.backend.mybatis.entity.ZtLabel;
import cn.com.efuture.o2o.backend.mybatis.entity.ZtLabelType;
import cn.com.efuture.o2o.backend.mybatis.service.ZtLabelServiceImpl;
import cn.com.efuture.o2o.backend.system.JsonResponse;

@RestController
@RequestMapping("/api/ztLabel")
public class ZtLabelContorller {

	protected org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final ZtLabelServiceImpl ztLabelServiceImpl;
	
	public ZtLabelContorller(ZtLabelServiceImpl ztLabelServiceImpl) {
		super();
		this.ztLabelServiceImpl=ztLabelServiceImpl;
	}
	
    /**
     *  获取所有标签
     *
     * @return ZtLabelList
     */
    @RequestMapping(value = "/getZtLabelList", method = RequestMethod.GET)
    public JsonResponse getZtLabelList(@RequestParam(value = "data") String data) {
    	Map<String, Object> map = JSONObject.parseObject(data);
        try {
            logger.info("------------getZtLabelList-----------");
            //执行查询
            List<ZtLabelType> list = ztLabelServiceImpl.getZtLabelTypeList(map);
            return JsonResponse.ok(list);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }

    /**
     *  新增标签
     * @param data ztLabel
     * @return JsonResponse
     */
    @RequestMapping(value = "/insertZtLabel", method = RequestMethod.POST)
    public JsonResponse insertZtLabel(@RequestParam(value = "data") String data) {
        try {
            logger.info("------------insertZtLabel-----------");
            ZtLabel ztLabel = JSONObject.parseObject(data, ZtLabel.class);
            ztLabelServiceImpl.insertZtLabel(ztLabel);
            return JsonResponse.ok("新增成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
    
    
    /**
     *  修改标签
     * @param data ztLabel
     * @return JsonResponse
     */
    @RequestMapping(value = "/updateZtLabel", method = RequestMethod.POST)
    public JsonResponse updateZtLabel(@RequestParam(value = "data") String data) {
        try {
            logger.info("------------updateShopLogistics-----------");
            ZtLabel ztLabel = JSONObject.parseObject(data, ZtLabel.class);
            ztLabelServiceImpl.updateZtLabel(ztLabel);
            return JsonResponse.ok("修改成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }

    /**
     *  删除标签
     * @param data ztLabel
     * @return JsonResponse
     */
    @RequestMapping(value = "/deleteZtLabel", method = RequestMethod.POST)
    public JsonResponse deleteZtLabel(@RequestParam(value = "data") String data) {
        try {
            logger.info("------------deleteZtLabel-----------");
            ZtLabel ztLabel = JSONObject.parseObject(data, ZtLabel.class);
            ztLabelServiceImpl.deleteZtLabel(ztLabel);
            return JsonResponse.ok("删除成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
        }
    }
}
