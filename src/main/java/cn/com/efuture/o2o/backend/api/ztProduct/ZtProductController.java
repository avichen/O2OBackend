package cn.com.efuture.o2o.backend.api.ztProduct;

import cn.com.efuture.o2o.backend.mybatis.entity.ZtProduct;
import cn.com.efuture.o2o.backend.mybatis.service.ZtProductServiceImpl;
import cn.com.efuture.o2o.backend.system.JsonResponse;
import cn.com.efuture.o2o.backend.util.ParameterHelper;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/ztProduct")
public class ZtProductController {

    protected org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());


    private final ZtProductServiceImpl ztProductService;


    public ZtProductController(ZtProductServiceImpl ztProductService) {
        this.ztProductService = ztProductService;
    }

    @GetMapping(value = "getZtSkuList")
    public JsonResponse getZtSkuList(@RequestParam(value = "data") String data) {
        logger.info("------------getZtSkuList-----------");
        JSONObject map = JSONObject.parseObject(data);
        try {
            ParameterHelper.cookPageInfo(map);
//            map.put("userName", SessionHelper.getUserName());
            long count = ParameterHelper.getCount(map, ztProductService,"getZtSkuList");
            // 设置分页信息
            int page = map.getIntValue("page");
            int pageSize = map.getIntValue("pageSize");
            map.put("page", (page - 1) * pageSize);
            map.put("pageSize", pageSize);
            //执行查询
            List<Map<String, Object>> ztSkuList = ztProductService.getZtSkuList(map);
            //获取查询结果
            return JsonResponse.ok(count,ztSkuList);
        } catch (Exception e) {
            return JsonResponse.SERVER_ERR;
        }
    }

    @RequestMapping(value = "/getZtProductList", method = RequestMethod.GET)
    public JsonResponse getZtProductList(@RequestParam(value = "data") String data) {
        logger.info("------------getZtProductList-----------");
        JSONObject map = JSONObject.parseObject(data);
        try {
            ParameterHelper.cookPageInfo(map);
//            map.put("userName", SessionHelper.getUserName());
            long count = ParameterHelper.getCount(map, ztProductService,"getZtProductList");
            // 设置分页信息
            int page = map.getIntValue("page");
            int pageSize = map.getIntValue("pageSize");
            map.put("page", (page - 1) * pageSize);
            map.put("pageSize", pageSize);
            //执行查询
            List<ZtProduct> list = ztProductService.getZtProductList(map);
            //获取查询结果
            return JsonResponse.ok(count,list);
        } catch (Exception e) {
            return JsonResponse.SERVER_ERR;
        }
    }


    @RequestMapping(value = "/getZtProduct", method = RequestMethod.GET)
    public JsonResponse getZtProduct(@RequestParam(name = "data") String data) {
        logger.info("------------getZtProduct-----------");
        JSONObject map = JSONObject.parseObject(data);
        try {
            ZtProduct ztProduct = ztProductService.getZtProductByItemId(map);
            return JsonResponse.ok(ztProduct);
        } catch (Exception e) {
            return JsonResponse.SERVER_ERR;
        }
    }

    @RequestMapping(value = "/updateZtProduct",method = RequestMethod.POST)
    public JsonResponse updateZtProduct(@RequestParam(name = "data") String data) {
        logger.info("------------updateZtProduct-----------");
        ZtProduct ztProduct = JSONObject.parseObject(data,ZtProduct.class);
        try {
            ztProductService.updateZtProduct(ztProduct);
            return JsonResponse.ok("success");
        } catch (Exception e) {
            return JsonResponse.SERVER_ERR;
        }
    }


    @RequestMapping(value = "/getZtProductImagesList", method = RequestMethod.GET)
    public JsonResponse getZtProductImagesList(@RequestParam(value = "data") String data) {
        logger.info("------------getZtProductImagesList-----------");
        JSONObject map = JSONObject.parseObject(data);
        try {
            //执行查询
            List<Map> list = ztProductService.getZtProductImagesList(map);
            //获取查询结果
            return JsonResponse.ok(list);
        } catch (Exception e) {
            return JsonResponse.SERVER_ERR;
        }
    }

    @GetMapping(value = "getZtProductLabelList")
    public JsonResponse getZtProductLabelList(@RequestParam(value = "data") String data) {
        logger.info("------------getZtProductLabelList-----------");
        JSONObject map = JSONObject.parseObject(data);
        try {
            //执行查询
            List<Map> list = ztProductService.getZtProductLabelList(map);
            //获取查询结果
            return JsonResponse.ok(list);
        } catch (Exception e) {
            return JsonResponse.SERVER_ERR;
        }
    }

    @PostMapping(value = "saveZtProductLabelList")
    public JsonResponse saveZtProductLabelList(@RequestParam(value = "data") String data) {
        logger.info("------------saveZtProductLabelList-----------");
        JSONObject map = JSONObject.parseObject(data);
        try {
            //执行查询
            ztProductService.saveZtProductLabelList(map);
            //获取查询结果
            return JsonResponse.ok("success");
        } catch (Exception e) {
            return JsonResponse.SERVER_ERR;
        }
    }
}
