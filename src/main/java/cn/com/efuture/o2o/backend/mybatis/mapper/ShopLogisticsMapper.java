package cn.com.efuture.o2o.backend.mybatis.mapper;

import cn.com.efuture.o2o.backend.mybatis.entity.ShopLogistics;
import cn.com.efuture.o2o.backend.mybatis.entity.ShopLogisticsTime;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ShopLogisticsMapper {

    List<ShopLogistics> getShopLogisticsList(Map<String, Object> map);
    
    ShopLogistics getShopLogistics(Map<String, Object> map);

    List<ShopLogisticsTime> getShopLogisticsTimeList(Map<String, Object> map);

    void insertShopLogistics(Map<String, Object> map);
    
    void updateShopLogistics(Map<String, Object> map);
    
    void insertShopLogisticsTime(Map<String, Object> map);
    
    void updateShopLogisticsTime(Map<String, Object> map);
    
    void deleteShopLogisticsTime(Map<String, Object> map);
}