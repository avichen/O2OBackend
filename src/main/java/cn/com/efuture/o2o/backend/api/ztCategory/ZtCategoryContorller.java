package cn.com.efuture.o2o.backend.api.ztCategory;

import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.com.efuture.o2o.backend.mybatis.entity.ZtCategory;
import cn.com.efuture.o2o.backend.mybatis.service.ZtCategoryServiceImpl;
import cn.com.efuture.o2o.backend.system.JsonResponse;
import cn.com.efuture.o2o.backend.util.SessionHelper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/ztCategory")
public class ZtCategoryContorller {

	protected org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());
	
	 private final ZtCategoryServiceImpl ztCategoryServiceImpl;
	 
	 public ZtCategoryContorller(ZtCategoryServiceImpl ztCategoryServiceImpl) {
			super();
			this.ztCategoryServiceImpl = ztCategoryServiceImpl;
		}
	 
		@ApiOperation(value = "获取中台类别清单", notes = "获取中台类别清单")
	    @ApiImplicitParams({
	            @ApiImplicitParam(name = "data", value = "{}", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "categoryId", value = "类别编码", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "categoryName", value = "类别名称", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "parentCategoryId", value = "上级类别编码", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "level", value = "类别级别", dataType = "Int"),
	            @ApiImplicitParam(name = "seqNo", value = "排序级别", dataType = "Int"),
	            @ApiImplicitParam(name = "flag", value = "状态", dataType = "Int"),
	            @ApiImplicitParam(name = "lastModifyTime", value = "最后修改时间", dataType = "date")
	    })
	    @RequestMapping(value = "/getZtCategoryList", method = RequestMethod.GET)
	    public JsonResponse getZtCategoryList(@RequestParam(value = "data") String data) {
	        logger.info("------------getZtCategoryList-----------");
	        Map<String, Object> map = JSONObject.parseObject(data);
	        try {
	            //执行查询
	            List<ZtCategory> list = ztCategoryServiceImpl.getZtCategoryList(map);
	            //获取查询结果
	            return JsonResponse.ok(list);
	        } catch (Exception e) {
	            logger.debug(e.toString());
	            return JsonResponse.SERVER_ERR;
	        }
	    }


	    @ApiOperation(value = "新增中台类别", notes = "新增中台类别")
	    @ApiImplicitParams({
	            @ApiImplicitParam(name = "data", value = "{}", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "categoryId", value = "类别编码", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "categoryName", value = "类别名称", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "parentCategoryId", value = "上级类别编码", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "level", value = "类别级别", dataType = "Int"),
	            @ApiImplicitParam(name = "seqNo", value = "排序级别", dataType = "Int"),
	            @ApiImplicitParam(name = "flag", value = "状态", dataType = "Int"),
	            @ApiImplicitParam(name = "lastModifyTime", value = "最后修改时间", dataType = "date")
	    })
	    @RequestMapping(value = "/insertZtCategory", method = RequestMethod.POST)
	    public JsonResponse insertZtCategory(@RequestParam(value = "data") String data) {
	        logger.info("------------insertZtCategory-----------");
	        ZtCategory ztCategory = JSONObject.parseObject(data, ZtCategory.class);
	        //String userName = SessionHelper.getUserName();
	        try {
	        	if(ztCategory.getCategoryName() != null && ztCategory.getCategoryName().length()>10) {
	        		return JsonResponse.notOk(500, "类别名称最大长度为10");
	        	}
	        	if(ztCategory.getMemo() != null && ztCategory.getMemo().length()>20) {
	        		return JsonResponse.notOk(500, "类别备注最大长度为20");
	        	}
	            //执行新增
	            ztCategoryServiceImpl.insertZtCategory(ztCategory,"test");
	            return JsonResponse.ok("新增成功");
	        } catch (Exception e) {
	        	logger.error(e.getMessage());
	            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
	        }
	    }

	    @ApiOperation(value = "修改类别", notes = "修改类别")
	    @ApiImplicitParams({
	            @ApiImplicitParam(name = "data", value = "{}", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "categoryId", value = "本地类别编码", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "categoryName", value = "类别名称", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "seqNo", value = "排序级别", dataType = "Int"),
	            @ApiImplicitParam(name = "flag", value = "状态", dataType = "Int"),
	            @ApiImplicitParam(name = "lastModifyTime", value = "最后修改时间", dataType = "date")
	    })
	    @RequestMapping(value = "/updateZtCategory", method = RequestMethod.POST)
	    public JsonResponse updateZtCategory(@RequestParam(value = "data") String data) {
	        logger.info("------------updateZtCategory-----------");
	        ZtCategory ztCategory = JSONObject.parseObject(data, ZtCategory.class);
	        //String userName = SessionHelper.getUserName();
	        
	        try {
	        	if(ztCategory.getCategoryName() != null && ztCategory.getCategoryName().length()>10) {
	        		return JsonResponse.notOk(500, "类别名称最大长度为10");
	        	}
	        	if(ztCategory.getMemo() != null && ztCategory.getMemo().length()>20) {
	        		return JsonResponse.notOk(500, "类别备注最大长度为20");
	        	}
	            //执行修改
	            ztCategoryServiceImpl.updateZtCategory(ztCategory,"test");
	            return JsonResponse.ok("修改成功");
	        } catch (Exception e) {
	        	logger.error(e.getMessage());
	            return JsonResponse.notOk(JsonResponse.ERR, e.getMessage());
	        }
	    }

	    @ApiOperation(value = "修改类别排序", notes = "修改类别排序")
	    @ApiImplicitParams({
	            @ApiImplicitParam(name = "data", value = "{}", required = true, dataType = "Json"),
	            @ApiImplicitParam(name = "categoryId", value = "本地类别编码", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "categoryName", value = "类别名称", required = true, dataType = "String"),
	            @ApiImplicitParam(name = "seqNo", value = "排序级别", dataType = "Int"),
	            @ApiImplicitParam(name = "lastModifyTime", value = "最后修改时间", dataType = "date")
	    })
	    @RequestMapping(value = "/updateZtCategoryList", method = RequestMethod.POST)
	    public JsonResponse updateZtCategoryList(@RequestParam(value = "data") String data) {
	        logger.info("------------updateZtCategoryList-----------");
	        List<ZtCategory> ztCategorys = JSONObject.parseArray(data, ZtCategory.class);
	        String userName = SessionHelper.getUserName();
	        try {
	            //执行修改
	            for (ZtCategory ztCategory : ztCategorys) {
	                ztCategoryServiceImpl.updateZtCategory(ztCategory,userName);
	            }
	            return JsonResponse.ok("修改成功");
	        } catch (Exception e) {
	            logger.debug(e.toString());
	            return JsonResponse.SERVER_ERR;
	        }
	    }
}
