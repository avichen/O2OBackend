package cn.com.efuture.o2o.backend.mybatis.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import cn.com.efuture.o2o.backend.mybatis.entity.Dept;
import cn.com.efuture.o2o.backend.mybatis.entity.ZtCategory;
import cn.com.efuture.o2o.backend.mybatis.entity.ZtCategoryLog;
import cn.com.efuture.o2o.backend.mybatis.mapper.ZtCategoryMapper;

@Service
public class ZtCategoryServiceImpl {
	    
	    private final ZtCategoryMapper ztCategoryMapper; 

	    public ZtCategoryServiceImpl(ZtCategoryMapper ztCategoryMapper) {
			super();
			this.ztCategoryMapper = ztCategoryMapper;
		}

		public List<ZtCategory> getZtCategoryList(Map<String, Object> map) {
	        return ztCategoryMapper.getZtCategoryList(map);
	    }

	    public ZtCategory getZtCategory(Map<String, Object> map) {
	        return ztCategoryMapper.getZtCategory(map);
	    }

	    public int getZtCategoryIdCount(ZtCategory ztCategory) {
	        return ztCategoryMapper.getZtCategoryIdCount(ztCategory);
	    }

	    public int getSeqNoMax(ZtCategory ztCategory) {
	        return ztCategoryMapper.getSeqNoMax(ztCategory);
	    }

	    public void insertZtCategory(ZtCategory ztCategory,String userName) {
	        ZtCategoryLog ztCategoryLog = new ZtCategoryLog();
	        //生成新CategoryId
	        String cid = String.format("%02d", ztCategoryMapper.getZtCategoryIdCount(ztCategory) + 1);

	        //判断该上级类别下是否有子类别
	        if (ztCategoryMapper.getZtCategoryIdCount(ztCategory) == 0) {
	        	ztCategory.setSeqNo(1);
	        } else {
	        	ztCategory.setSeqNo(ztCategoryMapper.getSeqNoMax(ztCategory) + 1);
	        }
	        //判断是否为上级类别
	        if (ztCategory.getParentCategoryId().equals("0")) {
	            //生成上级类别CategoryId
	        	ztCategory.setCategoryId(cid);
	        	ztCategory.setLevel(1);
	        } else {
	            //生成子类别CategoryId
	        	ztCategory.setCategoryId(ztCategory.getParentCategoryId() + cid);
	        	ztCategory.setLevel(2);
	        }
	        //写入本地类别和渠道类别表
	        ztCategoryLog.setCategoryId(ztCategory.getCategoryId());
	        ztCategoryMapper.insertZtCategory(ztCategory);
	        //写入类别日志
	        ztCategoryLog.setOperator(userName);
	        ztCategoryLog.setCategoryId(ztCategory.getCategoryId());
	        ztCategoryLog.setCategoryName(ztCategory.getCategoryName());
	        ztCategoryLog.setTaskType(1);
	        ztCategoryLog.setProcessStatus(0);
	        ztCategoryMapper.insertZtCategoryLog(ztCategoryLog);
	    }

	    public void updateZtCategory(ZtCategory ztCategory,String userName) {
	    	//执行修改
	    	ztCategoryMapper.updateZtCategory(ztCategory);
	    	ZtCategoryLog ztCategoryLog = new ZtCategoryLog(); 
	        //判断是否修改禁用状态
	        if (ztCategory.getFlag() !=null) {
	        	ztCategoryLog.setTaskType(3);
	        }else {
	        //修改类别名称或排序时写入类别日志
	          if (ztCategory.getCategoryName() != null) {
	        	  ztCategoryLog.setTaskType(2);
	        }else if (ztCategory.getSeqNo() !=null) {
	        	ztCategoryLog.setTaskType(4);
	        } 
	    }
	        ztCategoryLog.setCategoryId(ztCategory.getCategoryId());
	        ztCategoryLog.setOperator(userName);
	        ztCategoryLog.setProcessStatus(0);
	        ztCategoryMapper.insertZtCategoryLog(ztCategoryLog);  
	        
	}
	    //获取业务系统类别
	    public Dept getDept(Map<String, Object> map) {
	        return ztCategoryMapper.getDept(map);
	    }
	    

	}

