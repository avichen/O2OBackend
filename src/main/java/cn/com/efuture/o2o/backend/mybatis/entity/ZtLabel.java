package cn.com.efuture.o2o.backend.mybatis.entity;

public class ZtLabel {

	private Long labelId;
	private String labelTypeId;
	private String labelName;
	private String labelNameEn;
	private String categoryId;
	private int seqNo;
	
	public Long getLabelId() {
		return labelId;
	}
	public void setLabelId(Long labelId) {
		this.labelId = labelId;
	}
	public String getLabelName() {
		return labelName;
	}
	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}
	public String getLabelNameEn() {
		return labelNameEn;
	}
	public void setLabelNameEn(String labelNameEn) {
		this.labelNameEn = labelNameEn;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getLabelTypeId() {
		return labelTypeId;
	}
	public void setLabelTypeId(String labelTypeId) {
		this.labelTypeId = labelTypeId;
	}
	public int getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	
	
}
